//
//  MyVehicleApp.swift
//  MyVehicle
//
//  Created by Andrii Yarmolovskyi on 20.07.2023.
//

import SwiftUI

@main
struct MyVehicleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
